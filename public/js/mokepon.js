let playerAttack = []
let enemyAttack = []
let player
let playerId = null
let enemyId = null
let enemyWins = 0
let playerWins = 0
let battleResult = document.getElementById("result")

const apiUrl = 'http://192.168.1.105:8080/'
//Sections
const attackSection = document.getElementById("attack-selection")
const restartSection = document.getElementById("restart-section")

//Attack buttons
const attackContainer = document.getElementById("attack-container")
let rockButton
let waterButton
let fireButton
let buttonAtt = []

//Action buttons
const buttonPetSelection = document.getElementById("select-pet")
const restartButton = document.getElementById("restart-button")

//pets
const cardContainer = document.getElementById("card-container")
const enemyPetSelection = document.getElementById("enemy-pet")
const petSection = document.getElementById("pet-selection")
const petPlayerSpan = document.getElementById('player-pet')

//span lifes
const spanPlayerLife = document.getElementById('player-life')
const spanEnemyLife = document.getElementById('enemy-life')

//messages
const msjPlayerAttack = document.getElementById("player-attack")
const msjEnemyAttack = document.getElementById("enemy-attack")

//map  canvas
const mapSection = document.getElementById('map-view')
const map = document.getElementById('map')
let lienzo = map.getContext('2d')
let interval
let mapBackground = new Image()
mapBackground.src = './assets/mokemap.png'
let heightY
let mapWidth = window.innerWidth - 20
const maxWidth = 950

if (mapWidth > maxWidth) {
    mapWidth = maxWidth - 20
}
heightY = mapWidth * 550 / maxWidth
map.width = mapWidth
map.height = heightY

class Mokepon {
    constructor(name, picture, life, id = null) {
        this.id = id
        this.name = name
        this.picture = picture
        this.life = life
        this.attacks = []
        this.width = map.width / 10
        this.height = map.width / 10
        this.x = random_select(0, map.width - this.width)
        this.y = random_select(0, map.height - this.height)
        this.putImage = new Image()
        this.putImage.src = picture
        this.speedX = 0
        this.speedY = 0
    }

    drawMokepon() {
        lienzo.drawImage(
            this.putImage,
            this.x,
            this.y,
            this.width,
            this.height
        )
    }
}

//mokepones array
let mokepones = []
let mokeponOptions
let attackOptions
let mokeponObject = []
let enemyMokeponDraw = []
//input radio de mokepones
let requasPet
let sorbabolPet
let dermancharPet
//ataques del mokepon seleccionado
let mokeponAttacks
let enemyMokeponAttacks = []
let indexPlayerAttack
let indexEnemyAttack
//Mokepones registrados
let Requas = new Mokepon('Requas', './assets/square.png', 5)
let Sorbabol = new Mokepon('Sorbabol', './assets/pngegg.png', 5)
let Dermanchar = new Mokepon('Dermanchar', './assets/Dermanchar.png', 5)

const REQUAS_ATTACKS = [
    { name: 'AGUA 💧', id: 'water-button' },
    { name: 'AGUA 💧', id: 'water-button' },
    { name: 'AGUA 💧', id: 'water-button' },
    { name: 'FUEGO 🔥', id: 'fire-button' },
    { name: 'TIERRA 🌱', id: 'rock-button' }
]


const SORBABOL_ATTACKS = [
    { name: 'TIERRA 🌱', id: 'rock-button' },
    { name: 'TIERRA 🌱', id: 'rock-button' },
    { name: 'TIERRA 🌱', id: 'rock-button' },
    { name: 'AGUA 💧', id: 'water-button' },
    { name: 'FUEGO 🔥', id: 'fire-button' }
]


const DERMANCHAR_ATTACKS = [
    { name: 'FUEGO 🔥', id: 'fire-button' },
    { name: 'FUEGO 🔥', id: 'fire-button' },
    { name: 'FUEGO 🔥', id: 'fire-button' },
    { name: 'AGUA 💧', id: 'water-button' },
    { name: 'TIERRA 🌱', id: 'rock-button' }
]
Requas.attacks.push(...REQUAS_ATTACKS)

Sorbabol.attacks.push(...SORBABOL_ATTACKS)

Dermanchar.attacks.push(...DERMANCHAR_ATTACKS)


mokepones.push(Requas, Sorbabol, Dermanchar)
//let attacks = ['FUEGO', 'TIERRA', 'AGUA']

function startGame() {
    attackSection.style.display = "none"
    restartSection.style.display = "none"
    mapSection.style.display = "none"

    mokepones.forEach(mokepon => {
        mokeponOptions = `
        <input type="radio" name="pets" id=${mokepon.name} />
        <label class="mokepon-card" for=${mokepon.name}>
            <p>${mokepon.name}</p>
            <img src=${mokepon.picture} alt=${mokepon.name}>
        </label>
        `
        cardContainer.innerHTML += mokeponOptions
        requasPet = document.getElementById('Requas')
        sorbabolPet = document.getElementById('Sorbabol')
        dermancharPet = document.getElementById('Dermanchar')
    });

    buttonPetSelection.addEventListener('click', petSelection)
    restartButton.addEventListener('click', restartGame)

    joinToGame()
}

function joinToGame() {
    fetch(apiUrl + 'join')
        .then((response) => {
            if (response.ok) {
                response.text()
                    .then((res) => {
                        console.log(res)
                        playerId = res
                    })
            }
        })
}

function petSelection() {

    if (requasPet.checked) {
        player = requasPet.id
    } else if (sorbabolPet.checked) {
        player = sorbabolPet.id
    } else if (dermancharPet.checked) {
        player = dermancharPet.id
    } else if (requasPet.checked) {
        player = 'Requas'
    } else {
        alert('Debes seleccionar un Mokepon')
        return
    }
    console.log('ENTRE')
    petPlayerSpan.innerHTML = player

    mokepones.forEach((mokepon) => {
        if (player == mokepon.name) {
            mokeponObject = mokepon
            mokeponAttacks = mokepon.attacks
        }
    })

    selectMokepon(mokeponObject.name)
    petSection.style.display = "none"
    mapSection.style.display = "flex"
    startMap()
    //enemySelection()
    

}

function selectMokepon(mok) {
    fetch(apiUrl + `mokepon/${playerId}`, {
        method: 'post',
        headers: {
            "Content-type": "application/json"
        },
        body: JSON.stringify({
            mokepon: mok
        })
    })
}

function enemySelection(enemyMokepon) {
    //let enemyMokepon = random_select(0, mokepones.length - 1)
    enemyPetSelection.innerHTML = enemyMokepon.name
    enemyMokeponAttacks = enemyMokepon.attacks
    enemyMokeponAttacks.sort(() => Math.random() - 0.5)

    addAttackButton()
    attackSecuence()
}

function addAttackButton() {
    mokeponAttacks.forEach((attack) => {
        attackOptions = `
            <button id=${attack.id} class="attack-button BAttack">${attack.name}</button>
            `
        attackContainer.innerHTML += attackOptions
    })
    rockButton = document.getElementById("rock-button")
    waterButton = document.getElementById("water-button")
    fireButton = document.getElementById("fire-button")
    buttonAtt = document.querySelectorAll('.BAttack')
}

function attackSecuence() {
    buttonAtt.forEach(button => {
        button.addEventListener('click', (e) => {
            if (e.target.textContent == 'FUEGO 🔥') {
                playerAttack.push('FUEGO')
                button.style.background = '#091731'
                button.disabled = true
            } else if (e.target.textContent == 'AGUA 💧') {
                playerAttack.push('AGUA')
                button.style.background = '#091731'
                button.disabled = true
            } else if (e.target.textContent == 'TIERRA 🌱') {
                playerAttack.push('TIERRA')
                button.style.background = '#091731'
                button.disabled = true
            }
            if (playerAttack.length === 5) {
                sendAttack()
                console.log(playerAttack)
            }
        })
    })
}

function sendAttack() {
    fetch(apiUrl + `mokepon/${playerId}/attacks`, {
        method: 'post',
        headers: {
            "Content-type": "application/json"
        },
        body: JSON.stringify({
            attacks: playerAttack
        })
    })
    interval = setInterval(getAttacks, 50)
}

function getAttacks() {
    console.log("check")
    fetch(apiUrl + `mokepon/${enemyId}/attacks`)
        .then((res) => {
            if (res.ok) {
                res.json()
                    .then(({attacks}) => {
                        if (attacks.length === 5) {
                            console.log("LISTO")
                            enemyAttack = attacks
                            battle()
                        }
                    })
            }
        })
}

function indexAttacks(index) {
    indexPlayerAttack = playerAttack[index]
    indexEnemyAttack = enemyAttack[index]
}

function battle() {
    clearInterval(interval)

    for (let i = 0; i < playerAttack.length; i++) {
        if (playerAttack[i] == enemyAttack[i]) {
            indexAttacks(i)
            createMessage('EMPATE')
        } else if (playerAttack[i] == 'FUEGO' && enemyAttack[i] == 'TIERRA' || playerAttack[i] == 'AGUA' && enemyAttack[i] == 'FUEGO' || playerAttack[i] == 'TIERRA' && enemyAttack[i] == 'AGUA') {
            indexAttacks(i)
            playerWins++
            spanPlayerLife.innerHTML = playerWins
            createMessage('GANASTE')
        } else {
            indexAttacks(i)
            enemyWins++
            spanEnemyLife.innerHTML = enemyWins
            createMessage('PERDISTE')
        }
        console.log('wins: ', playerWins, enemyWins)
    }

    checkWins()
}

function checkWins() {
    if (enemyWins == playerWins) {
        createFinalMessage("Empate")
    } else if (playerWins > enemyWins) {
        createFinalMessage("Has GANADO la batalla")
    } else {
        createFinalMessage("Has PERDIDO la batalla")
    }
}

function createFinalMessage(finalResult) {
    battleResult.innerHTML = finalResult

    restartSection.style.display = "block"
}

function createMessage(result) {
    let newPlayerAttack = document.createElement("p")
    let newEnemyAttack = document.createElement("p")

    newEnemyAttack.innerHTML = indexEnemyAttack
    newPlayerAttack.innerHTML = indexPlayerAttack

    battleResult.innerHTML = result
    msjPlayerAttack.appendChild(newPlayerAttack)
    msjEnemyAttack.appendChild(newEnemyAttack)

    console.log(indexPlayerAttack, indexEnemyAttack, result)
}

function restartGame() {
    location.reload()
}

function random_select(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min)
}

function drawWorld() {
    mokeponObject.x = mokeponObject.x + mokeponObject.speedX
    mokeponObject.y = mokeponObject.y + mokeponObject.speedY

    lienzo.clearRect(0, 0, map.width, map.height)
    lienzo.drawImage(
        mapBackground,
        0,
        0,
        map.width,
        map.height
    )
    mokeponObject.drawMokepon()

    sendPosition(mokeponObject.x, mokeponObject.y)

    enemyMokeponDraw.forEach((mokepon) => {
        mokepon.drawMokepon()
        checkCollision(mokepon)
    })
}

function sendPosition(x, y) {
    fetch(apiUrl + `mokepon/${playerId}/position`, {
        method: 'post',
        headers: {
            "Content-type": "application/json"
        },
        body: JSON.stringify({
            x,
            y
        })
    })
        .then((response) => {
            if (response.ok) {
                response.json()
                    .then(({ enemies }) => {
                        console.log(enemies)
                        enemyMokeponDraw = enemies.map(enemy => {
                            let mokeponEnemy = null
                            const mokeponName = enemy.mokepon.name || ""
                            if (mokeponName === 'Requas') {
                                mokeponEnemy = new Mokepon('Requas', './assets/square.png', 5, enemy.id)
                            } else if (mokeponName === 'Sorbabol') {
                                mokeponEnemy = new Mokepon('Sorbabol', './assets/pngegg.png', 5, enemy.id)
                            } else if (mokeponName === 'Dermanchar') {
                                mokeponEnemy = new Mokepon('Dermanchar', './assets/Dermanchar.png', 5, enemy.id)
                            }


                            mokeponEnemy.x = enemy.x
                            mokeponEnemy.y = enemy.y

                            return mokeponEnemy
                        });
                    })
            }
        })
}

function moveMokepon(direction) {
    if (direction == 1) {
        mokeponObject.speedY = -10
    } else if (direction == 2) {
        mokeponObject.speedX = 10
    } else if (direction == 3) {
        mokeponObject.speedY = 10
    } else if (direction == 4) {
        mokeponObject.speedX = -10
    }
}

function stopMove() {
    mokeponObject.speedX = 0
    mokeponObject.speedY = 0
}

function pressKey(event) {
    switch (event.key) {
        case 'ArrowUp':
            moveMokepon(1)
            break
        case 'w':
            moveMokepon(1)
            break
        case 'ArrowRight':
            moveMokepon(2)
            break
        case 'd':
            moveMokepon(2)
            break
        case 'ArrowDown':
            moveMokepon(3)
            break
        case 's':
            moveMokepon(3)
            break
        case 'ArrowLeft':
            moveMokepon(4)
            break
        case 'a':
            moveMokepon(4)
            break
        default:
            break;
    }
}

function startMap() {
    interval = setInterval(drawWorld, 50)
    window.addEventListener('keydown', pressKey)
    window.addEventListener('keyup', stopMove)
}

function checkCollision(enemy) {
    const enemyUp = enemy.y
    const enemyDown = enemy.y + enemy.height
    const enemyLeft = enemy.x
    const enemyRight = enemy.x + enemy.width

    const playerUp = mokeponObject.y
    const playerDown = mokeponObject.y + mokeponObject.height
    const playerLeft = mokeponObject.x
    const playerRight = mokeponObject.x + mokeponObject.width

    if (playerDown < enemyUp ||
        playerUp > enemyDown ||
        playerRight < enemyLeft ||
        playerLeft > enemyRight
    ) {
        return
    }
    stopMove()
    clearInterval(interval)
    enemyId = enemy.id
    attackSection.style.display = "flex"
    mapSection.style.display = 'none'
    enemySelection(enemy)

}
window.addEventListener('load', startGame)