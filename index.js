const express = require('express')
const cors = require('cors')

const app = express()
app.use(express.static('public'))
app.use(cors())
app.use(express.json())

let players = []

class Player {
    constructor(id) {
        this.id = id
    }

    assignMokepon(mokepon){
        this.mokepon = mokepon
    }

    position(x, y){
        this.x = x
        this.y = y
    }

    assignAttacks(attacks){
        this.attacks = attacks
    }
}

class Mokepon {
    constructor(name) {
        this.name = name
    }
}
app.get('/join', (req, res) => {
    const id = `${Math.random()}`
    const player = new Player(id)

    players.push(player)

    res.send(id)
})

app.post('/mokepon/:playerId', (req, res) => {
    const playerId = req.params.playerId || ""
    const name = req.body.mokepon || ""
    const mokepon = new Mokepon(name)
    const indexPlayer = players.findIndex((player) => playerId === player.id)

    if(indexPlayer >= 0){
        players[indexPlayer].assignMokepon(mokepon)
    }

    console.log(players)
    console.log(playerId)

    res.end()
})

app.post('/mokepon/:playerId/position', (req, res) => {
    const playerId = req.params.playerId || ""
    const x = req.body.x || 0
    const y = req.body.y || 0
    const indexPlayer = players.findIndex((player) => playerId === player.id)

    if(indexPlayer >= 0){
        players[indexPlayer].position(x, y)
    }
    
    const enemies = players.filter((player) => playerId !== player.id)
    res.send({
        enemies
    })
})

app.post('/mokepon/:playerId/attacks', (req, res) => {
    const playerId = req.params.playerId || ""
    const attacks = req.body.attacks || []
    const indexPlayer = players.findIndex((player) => playerId === player.id)

    if(indexPlayer >= 0){
        players[indexPlayer].assignAttacks(attacks)
    }

    res.end()
})

app.get('/mokepon/:playerId/attacks', (req, res) => {
    const playerId = req.params.playerId || ""
    const player = players.find((p) => p.id === playerId)

    res.send({
        attacks: player.attacks || []
    })
})
app.listen(8080, () => {
    console.log('Servidor funcionando')
})